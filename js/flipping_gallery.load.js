/**
 * @file
 * Provides Flipping Gallery loader.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Flipping Gallery object.
   *
   * @namespace
   */
  Drupal.flippingGallery = Drupal.flippingGallery || {
    direction: 'forward',
    selector: '> a',
    spacing: 10,
    showMaximum: 15,
    enableScroll: true,
    flipDirection: 'bottom',
    autoplay: false
  };

  /**
   * Attaches flippingGallery behaviour.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.flippingGallery = {
    attach: function () {
      var $fp = $('.flipping-gallery');
      var configs = $fp.data('configs');

      for (var config in configs) {
        if (configs.hasOwnProperty(config)) {
          Drupal.flippingGallery[config] = configs[config];
        }
      }

      $fp.flipping_gallery(Drupal.flippingGallery);
    }
  };

})(jQuery, Drupal);
